//[OBJECTIVE] Create a server-side app using Express Web Framework

//Relate this task to something that you do in a daily basis, like prepairing your favorite food.



//append the entire app to our node package manager

//--------------------------------------------------------------------
//---- 2 Ways to initialize/append a Node Package Manager-------//

/* 1. npm init - you need to fill the the questions ask and type yes to produce a package.json 

	2. init -y - package.json in produce upon initializing
 package.json -. the heart of every node project.
	- contains diff. metadata that describes the structure of the project.
 */
//-----------------------------------------------------------------------


/*"scripts": {  ----> this is the start script, instead of writing node index, we will use "npm start" in the terminal or whatever we want to use but we will include the word run (ex. "jump",  npm run jump, because jump (or whatever we wnat to use aside from start) is an UNCONVENTIONAL script). <----

    "jump": "node index.js"
  },

  SYNTAX: npm run <custom command>

  script -> use to declare and describe custom commands and keyword that can be used to execute this project with the correct runtime environment.

NOTE: "start" is globally recognize amongst node projects and frameforks as the default command script to execute a task/project.
*/




//[SECTION] Create a runtime environment that will automatically autofix all changed in our app

	
	//were going to use a utility called nodemon
	// with nodemon we don't have to restart the terminal to display changes
	//upon starting the entry point module with nodemon you will be able to append the apllication with the proper run time environment, allowiong you to save time and effort upon commiting changes to your app

//1. Identify the ingridients

const express = require("express")

//epress -> will be usedas the main component to create the server.
// we nee dto able to gather/acquire the uriliries and components needed that the express library will provide us.
//=> require -> directive used to get the library/components needed inside the module.

//prepare the environment where the 
console.log(`
	Welcome to our express API server
	───▄▀▀▀▀▀───▄█▀▀▀█▄
──▐▄▄▄▄▄▄▄▄██▌▀▄▀▐██
──▐▒▒▒▒▒▒▒▒███▌▀▐███
───▌▒▓▒▒▒▒▓▒██▌▀▐██
───▌▓▐▀▀▀▀▌▓─▀▀▀▀▀
`);